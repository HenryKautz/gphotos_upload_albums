# GPhotos Upload Albums

## Installation

```
git clone https://gitlab.com/HenryKautz/gphotos_upload_albums.git
cd gphotos_upload_albums
cp gphotos_upload_albums.sh ~/bin
```

Edit gphotos\_upload\_albums.sh to set SCRIPT to path to gphotos_upload_albums.py.

Install required python libraries if not already installed:

    pip install google-auth google_auth_oauthlib

Create an OAUTH token:

1. Go to the Google API Console.
2. Create a new project or select an existing one.
3. Click on "Enable APIs and Services" and search for "Google Photos Library API". Enable it.
4. Click "Create credentials" and choose "OAuth client ID".
5. Select "Desktop app" as the application type, enter a name, and click "Create".
6. Download the client_secret JSON file and save it as 
 ~/.credentials/gphotos/credentials.json.
7. Go to the Oauth Consent Screen and add yourself as a Test user.


## Use

Where the PATH argument (possibly repeated) is the path to a folder containing media and/or subfolders, 

    python gphotos_upload_albums.py {-dryrun -collab -debug -chunk N} PATH {PATH ...}

will upload the media to newly created albums in Google Photos.  The name of each album is
the name of the folder that contained the media.  Google Photos does not support nested albums, so if the folders are hierarchically organized the created albums will ignore the hierarchical structure. No album is created for folders that are empty or that only contain subfolders.

Link sharing and commenting is turned on for albums.  After all media is uploaded, the program prints HTML code that can be used to display an index of the
albums. 

Options: 

* __-chunk N__  &nbsp; Set the chunk size to N times the chunk granularity. Default value is 20 and currently Google Photos chunk granularity is 5,242,880, so the default chunk size is approximately 105 MB.  If your connectivity is poor then set N to a smaller number.
* __-dryrun__ &nbsp; Traverse the directories but do not actually upload
* __-collab__ &nbsp; Make the albums collaborative
* __-debug__ &nbsp; Force the uploads to fail on the second chunk in order to test resumable uploads

Note that the program always creates new albums, so if an album already exists with the same name, then Google Photos will end up containing two albums with the same name. 

### Author
Henry Kautz (henry.kautz@gmail.com). This code is free for anyone to 
use for any purpose.



